<?php

namespace AutoMapper;

class AutoMapperFactory
{
    public function __construct(
        private AutoMapperConfigurationFactory $factory
    )
    {
    }

    /**
     * @return AutoMapper
     * @throws \AutoMapper\Exception\AutoMapperConfigurationException
     */
    public function getInstance(): AutoMapper
    {
        $autoMapper = new AutoMapper();
        $autoMapper->setConfig(
            $this->factory->getConfig()
        );

        return $autoMapper;
    }
}
