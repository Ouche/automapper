<?php

namespace AutoMapper;

class MappingConfiguration
{
    /**
     * Dirty hack for callable typing.
     *
     * @var callable
     */
    private $callable;

    public function __construct(
        private string $source,
        private string $destination,
        callable $callable
    ) {
        $this->callable = $callable;
    }

    public function getCallable(): callable
    {
        return $this->callable;
    }

    public function getDestination(): string
    {
        return $this->destination;
    }

    public function getSource(): string
    {
        return $this->source;
    }
}
