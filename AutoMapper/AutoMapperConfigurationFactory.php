<?php

namespace AutoMapper;

use AutoMapper\Exception\AutoMapperConfigurationException;

class AutoMapperConfigurationFactory
{
    /**
     * @param AutoMapperConfigInterface[] $configurators
     */
    public function __construct(
        private iterable $configurators
    ) {
    }

    /**
     * @throws AutoMapperConfigurationException
     */
    public function getConfig(): array
    {
        $config = [];

        /** @var AutoMapperConfigInterface $configurator */
        foreach ($this->configurators as $configurator) {
            /** @var MappingConfiguration $destination */
            foreach ($configurator->getMappingConfigurations() as $destination) {
                if (isset($config[$destination->getSource()][$destination->getDestination()])) {
                    throw new AutoMapperConfigurationException(sprintf('Mapping already exists from %s to %s', $destination->getSource(), $destination->getDestination()));
                }

                $config[$destination->getSource()][$destination->getDestination()] = $destination->getCallable();
            }
        }

        return $config;
    }
}
