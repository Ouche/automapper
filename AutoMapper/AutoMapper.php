<?php

namespace AutoMapper;

use AutoMapper\Exception\AutoMapperException;
use AutoMapper\Exception\BadMappingTypeException;
use AutoMapper\Exception\MissingSourcePropertyException;
use Exception;
use ReflectionObject;

class AutoMapper
{
    public const DATATYPE_ARRAY = 'array';

    private array $config;

    /**
     * @throws AutoMapperException
     */
    public function map(array|object $source, string|object $destination): object
    {
        $sourceType = $this->getType($source);
        try {
            if (is_string($destination)) {
                $destinationType = $destination;
                $destination = new $destination();
            } else {
                $destinationType = $this->getType($destination);
            }

            if (isset($this->config[$sourceType][$destinationType])) {
                return call_user_func($this->config[$sourceType][$destinationType], $source);
            }

            if (is_array($source)) {
                return $this->mapArrayToObject($source, $destination);
            } else {
                return $this->mapObjectToObject($source, $destination);
            }
        } catch (Exception $e) {
            $sourceType = is_array($source) ? 'array' : get_class($source);
            $destinationType = is_string($destination) ? $destination : get_class($destination);

            throw new AutoMapperException(sprintf('Something went wrong when trying to map %s into %s: %s', $sourceType, $destinationType, $e->getMessage()), $e->getCode(), $e);
        }
    }

    /**
     * @throws AutoMapperException
     */
    public function mapMultiple(array $source, string|object $destination): array
    {
        $results = [];

        foreach ($source as $item) {
            $results[] = $this->map($item, $destination);
        }

        return $results;
    }

    protected function getType(array|object $value): string
    {
        return is_array($value) ? self::DATATYPE_ARRAY : get_class($value);
    }

    /**
     * @throws BadMappingTypeException
     * @throws MissingSourcePropertyException
     */
    protected function mapArrayToObject(array $data, object $destination): object
    {
        $destinationObject = new $destination();
        $destinationReflection = new ReflectionObject($destinationObject);

        foreach ($destinationReflection->getProperties() as $property) {
            $propertyName = $property->getName();
            $propertyType = $property->getType()?->getName();

            if (in_array($propertyName, array_keys($data))) {
                if (!$this->checkType($data[$propertyName], $propertyType)) {
                    $unexpectedType = is_object($data[$propertyName]) ? get_class($data[$propertyName]) : gettype($data[$propertyName]);

                    throw new BadMappingTypeException(sprintf('Cannot map %s into %s', $unexpectedType, $propertyType));
                }

                $property->setValue($destinationObject, $data[$propertyName]);
            } else {
                if (!$property->getType()?->allowsNull() && !$property->isInitialized($destinationObject)) {
                    throw new MissingSourcePropertyException(sprintf('Missing property %s in source : %s', $propertyName, json_encode($data)));
                }

                $property->setValue($destinationObject, null);
            }
        }

        return $destinationObject;
    }

    /**
     * @throws BadMappingTypeException
     * @throws MissingSourcePropertyException
     */
    protected function mapObjectToObject(object $data, object $destination): object
    {
        $dataReflection = new ReflectionObject($data);
        $dataProperties = [];

        foreach ($dataReflection->getProperties() as $dataProperty) {
            $dataProperties[$dataProperty->getName()] = $dataProperty->getValue($data);
        }

        return $this->mapArrayToObject($dataProperties, $destination);
    }

    protected function checkType($data, ?string $type): bool
    {
        /**
         * gettype() returns "integer" but ReflectionType::getName() returns "int".
         */
        $dataType = gettype($data);

        if ('integer' === $dataType) {
            $dataType = 'int';
        }

        return $data instanceof $type || $dataType === $type || 'mixed' === $type;
    }

    public function setConfig(array $config): void
    {
        $this->config = $config;
    }
}
