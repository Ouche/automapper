<?php

namespace AutoMapper;

interface AutoMapperConfigInterface
{
    public function getMappingConfigurations(): array;
}
